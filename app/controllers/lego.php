<?php
define('URL_BASE', "https://rebrickable.com/api/v3/lego/");
define('HEADERS', ['Authorization: key 32af553e350a53d33fe02520d95b7ec4']);

function rebrickable($path){
    $url = URL_BASE . $path;
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, HEADERS);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    curl_close($ch);
    if ($result === false) {
        print_r("ERROR: " . curl_error($ch));
        exit;
    }
    return $result;
}

function get_lego($twig){
    if(isset($_GET['set'])){
        $get=$_GET['set'];
        $set="sets/?search=".$get;
        $result = json_decode(rebrickable($set));
        $sets = $result->results;
        $data=[
            // Li paso els resultats
            'data'=>$sets,
            // Li paso tambe la paraula per buscar nomès perque es vegi
            'query'=>$get
        ];
        echo $twig->render("llista_lego.twig", $data);
    }else{
        // Si no esta buscant res, obre la plantilla de buscar
        echo $twig->render("buscar_lego.twig");
    }
}

function get_set($twig){
    if(!isset($_GET['set_num'])){
        _die();
    }else{
        $get=$_GET['set_num'];
        $set="sets/".$get."/";
        $part=$set."parts/";
        // Fem un cURL per la informació del set i un altre per les parts incloses
        $result = json_decode(rebrickable($set));
        $parts=json_decode(rebrickable($part)); 
        $data=[
            'set'=>$result,
            'parts'=>$parts->results
        ];
        echo $twig->render("detalls_lego.twig",$data);
    }
}

function get_part($twig){
    if(!isset($_GET['part_num'])){
        _die();
    }else{
        // Fem una crida per la informació de la peca sense colors, perquè
        // a vegades si li especifiques un color no et mostra tota la informació
        $get=$_GET['part_num'];
        $set1="parts/".$get."/";
        $result1 = json_decode(rebrickable($set1));

        // Una crida per la peca amb el color especificat per mostrar
        // la imatge amb el color
        $set_col="parts/".$get."/colors/".$_GET['part_col']."/";
        $result_col = json_decode(rebrickable($set_col));

        // I una crida per els sets que inclouen la peca amb el color
        $set2=$set_col."/sets/";
        $result2 = json_decode(rebrickable($set2));
        $result2=$result2->results;
        
        $data=[
            'part'=>$result1,
            'part_col'=>$result_col,
            'sets'=>$result2
        ];
        echo $twig->render("part_lego.twig",$data);
    }
}