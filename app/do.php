<?php
require_once "config.php";
echo "<a href='/lego/web/lego'>Buscar</a>";

// Twig initialization
$loader = new \Twig\Loader\FilesystemLoader(TEMPLATES_DIR);
$twig = new \Twig\Environment($loader);

// Load controllers
foreach(glob(APP_DIR.'/controllers/*.php') as $path){
    require_once $path;
}

// Do the magic
if(!isset($_REQUEST["action"])){
    sleep(1);
    exit;
}
$action=$_REQUEST['action'];
$functionName='get_'.$action;
if(!function_exists($functionName)){
    sleep(1);
    exit;
}
$functionName($twig);

function _die(){
    sleep(1);
    exit;
}